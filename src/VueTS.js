import Vue from 'vue';
import * as tf from '@tensorflow/tfjs';
import * as tfvis from '@tensorflow/tfjs-vis';

const convertToTensor2D = (data, inputAttr, labelsAttr) => {
  return tf.tidy(() => {
    tf.util.shuffle(data);

    const inputs = data.map(dt => dt[inputAttr]);
    const labels = data.map(dt => dt[labelsAttr]);

    const inputsTensor = tf.tensor2d(inputs, [inputs.length, 1]);
    const labelsTensor = tf.tensor2d(labels, [labels.length, 1]);

    const inputMax = inputsTensor.max();
    const inputMin = inputsTensor.min();
    const labelMax = labelsTensor.max();
    const labelMin = labelsTensor.min();

    const normalizedInputs = inputsTensor
      .sub(inputMin)
      .div(inputMax.sub(inputMin));
    const normalizedLabels = labelsTensor
      .sub(labelMin)
      .div(labelMax.sub(labelMin));

    return {
      inputs: normalizedInputs,
      labels: normalizedLabels,
      inputMax,
      inputMin,
      labelMax,
      labelMin,
    };
  });
};

export default {
  install() {
    Vue.prototype.$tf = {
      tf,
      tfvis,
      convertToTensor2D,
    };
  },
};

import Vue from 'vue';
import App from './App.vue';

import VueTS from './VueTS';

Vue.config.productionTip = false;

Vue.use(VueTS);

new Vue({
  render: h => h(App),
}).$mount('#app');
